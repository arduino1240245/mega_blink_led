//Preparado para el MEGA 2560

int led = 13;     //Pin del led que hay en el MEGA 2560


void setup() 
{
  // put your setup code here, to run once:

  pinMode (led, OUTPUT);      //Elijo el tipo de salida que va a tener el pin
}

void loop() 
{
  // put your main code here, to run repeatedly:

  digitalWrite (led, HIGH);
  delay (100);
  digitalWrite (led, LOW);
  delay (100);
}
